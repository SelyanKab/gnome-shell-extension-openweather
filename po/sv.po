# Swedish translation for gnome-shell-extension-openweather
# Copyright (C) 2011
# This file is distributed under the same license as the gnome-shell-extension-openweather package.
# Mauricio Barraza <mauricio@barraza.se>, 2011.
#
msgid ""
msgstr ""
"Project-Id-Version: 3.0\n"
"Report-Msgid-Bugs-To: https://gitlab.com/jenslody/gnome-shell-extension-"
"openweather/issues\n"
"POT-Creation-Date: 2017-04-02 23:47+0200\n"
"PO-Revision-Date: 2018-03-25 12:38+0200\n"
"Last-Translator: Morgan Antonsson <morgan.antonsson@gmail.com>\n"
"Language-Team: \n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: src/extension.js:174
msgid "..."
msgstr "..."

#: src/extension.js:384
msgid ""
"Openweathermap.org does not work without an api-key.\n"
"Either set the switch to use the extensions default key in the preferences "
"dialog to on or register at https://openweathermap.org/appid and paste your "
"personal key into the preferences dialog."
msgstr "Openweathermap.org kräver en API-nyckel.\n"
"Välj antingen standardnyckeln under inställningar eller registrera och hämta "
"en egen nyckel hos https://openweathermap.org/appid. Klistra sedan in den "
"under inställningar."

#: src/extension.js:442
msgid ""
"Dark Sky does not work without an api-key.\n"
"Please register at https://darksky.net/dev/register and paste your personal "
"key into the preferences dialog."
msgstr "Dark Sky kräver en personlig API-nyckel.\n"
"Registrera och hämta nyckel hos https://darksky.net/dev/register. "
"Klistra sedan in den under inställningar."

#: src/extension.js:515
#, javascript-format
msgid "Can not connect to %s"
msgstr "Kan inte nå %s"

#: src/extension.js:861 data/weather-settings.ui:447
msgid "Locations"
msgstr "Platser"

#: src/extension.js:876
msgid "Reload Weather Information"
msgstr "Uppdatera väderinformation"

#: src/extension.js:891
msgid "Weather data provided by:"
msgstr "Väderdata tillhandahålls av:"

#: src/extension.js:907
#, javascript-format
msgid "Can not open %s"
msgstr "Kan inte öppna %s"

#: src/extension.js:914
msgid "Weather Settings"
msgstr "Väderinställningar"

#: src/extension.js:978 src/prefs.js:1036
msgid "Invalid city"
msgstr "Felaktig plats"

#: src/extension.js:989
msgid "Invalid location! Please try to recreate it."
msgstr "Felaktig plats! Försök att skapa om den."

#: src/extension.js:1036 data/weather-settings.ui:773
msgid "°F"
msgstr "°F"

#: src/extension.js:1038 data/weather-settings.ui:774
msgid "K"
msgstr "K"

#: src/extension.js:1040 data/weather-settings.ui:775
msgid "°Ra"
msgstr "°Ra"

#: src/extension.js:1042 data/weather-settings.ui:776
msgid "°Ré"
msgstr "°Ré"

#: src/extension.js:1044 data/weather-settings.ui:777
msgid "°Rø"
msgstr "°Rø"

#: src/extension.js:1046 data/weather-settings.ui:778
msgid "°De"
msgstr "°De"

#: src/extension.js:1048 data/weather-settings.ui:779
msgid "°N"
msgstr "°N"

#: src/extension.js:1050 data/weather-settings.ui:772
msgid "°C"
msgstr "°C"

#: src/extension.js:1091
msgid "Calm"
msgstr "Lugnt"

#: src/extension.js:1094
msgid "Light air"
msgstr "Svag vind"

#: src/extension.js:1097
msgid "Light breeze"
msgstr "Svag vind"

#: src/extension.js:1100
msgid "Gentle breeze"
msgstr "Måttlig vind"

#: src/extension.js:1103
msgid "Moderate breeze"
msgstr "Måttlig vind"

#: src/extension.js:1106
msgid "Fresh breeze"
msgstr "Frisk vind"

#: src/extension.js:1109
msgid "Strong breeze"
msgstr "Frisk vind"

#: src/extension.js:1112
msgid "Moderate gale"
msgstr "Hård vind"

#: src/extension.js:1115
msgid "Fresh gale"
msgstr "Hård vind"

#: src/extension.js:1118
msgid "Strong gale"
msgstr "Mycket hård vind"

#: src/extension.js:1121
msgid "Storm"
msgstr "Storm"

#: src/extension.js:1124
msgid "Violent storm"
msgstr "Svår storm"

#: src/extension.js:1127
msgid "Hurricane"
msgstr "Orkan"

#: src/extension.js:1131
msgid "Sunday"
msgstr "Söndag"

#: src/extension.js:1131
msgid "Monday"
msgstr "Måndag"

#: src/extension.js:1131
msgid "Tuesday"
msgstr "Tisdag"

#: src/extension.js:1131
msgid "Wednesday"
msgstr "Onsdag"

#: src/extension.js:1131
msgid "Thursday"
msgstr "Torsdag"

#: src/extension.js:1131
msgid "Friday"
msgstr "Fredag"

#: src/extension.js:1131
msgid "Saturday"
msgstr "Lördag"

#: src/extension.js:1137
msgid "N"
msgstr "N"

#: src/extension.js:1137
msgid "NE"
msgstr "NO"

#: src/extension.js:1137
msgid "E"
msgstr "O"

#: src/extension.js:1137
msgid "SE"
msgstr "SO"

#: src/extension.js:1137
msgid "S"
msgstr "S"

#: src/extension.js:1137
msgid "SW"
msgstr "SV"

#: src/extension.js:1137
msgid "W"
msgstr "V"

#: src/extension.js:1137
msgid "NW"
msgstr "NV"

#: src/extension.js:1230 src/extension.js:1239 data/weather-settings.ui:810
msgid "hPa"
msgstr "hPa"

#: src/extension.js:1234 data/weather-settings.ui:811
msgid "inHg"
msgstr "inHg"

#: src/extension.js:1244 data/weather-settings.ui:812
msgid "bar"
msgstr "bar"

#: src/extension.js:1249 data/weather-settings.ui:813
msgid "Pa"
msgstr "Pa"

#: src/extension.js:1254 data/weather-settings.ui:814
msgid "kPa"
msgstr "kPa"

#: src/extension.js:1259 data/weather-settings.ui:815
msgid "atm"
msgstr "atm"

#: src/extension.js:1264 data/weather-settings.ui:816
msgid "at"
msgstr "at"

#: src/extension.js:1269 data/weather-settings.ui:817
msgid "Torr"
msgstr "Torr"

#: src/extension.js:1274 data/weather-settings.ui:818
msgid "psi"
msgstr "psi"

#: src/extension.js:1279 data/weather-settings.ui:819
msgid "mmHg"
msgstr "mmHg"

#: src/extension.js:1284 data/weather-settings.ui:820
msgid "mbar"
msgstr "mbar"

#: src/extension.js:1328 data/weather-settings.ui:794
msgid "m/s"
msgstr "m/s"

#: src/extension.js:1332 data/weather-settings.ui:793
msgid "mph"
msgstr "mph"

#: src/extension.js:1337 data/weather-settings.ui:792
msgid "km/h"
msgstr "km/h"

#: src/extension.js:1346 data/weather-settings.ui:795
msgid "kn"
msgstr "kn"

#: src/extension.js:1351 data/weather-settings.ui:796
msgid "ft/s"
msgstr "fot/s"

#: src/extension.js:1442
msgid "Loading ..."
msgstr "Laddar..."

#: src/extension.js:1446
msgid "Please wait"
msgstr "Vänligen vänta"

#: src/extension.js:1507
msgid "Cloudiness:"
msgstr "Molnighet:"

#: src/extension.js:1511
msgid "Humidity:"
msgstr "Fuktighet:"

#: src/extension.js:1515
msgid "Pressure:"
msgstr "Lufttryck:"

#: src/extension.js:1519
msgid "Wind:"
msgstr "Vind:"

#: src/darksky_net.js:159 src/darksky_net.js:288 src/openweathermap_org.js:350
#: src/openweathermap_org.js:451
msgid "Yesterday"
msgstr "Igår"

#: src/darksky_net.js:162 src/darksky_net.js:291 src/openweathermap_org.js:352
#: src/openweathermap_org.js:453
#, javascript-format
msgid "%d day ago"
msgid_plural "%d days ago"
msgstr[0] "%d dag sedan"
msgstr[1] "%d dagar sedan"

#: src/darksky_net.js:174 src/darksky_net.js:176 src/openweathermap_org.js:366
#: src/openweathermap_org.js:368
msgid ", "
msgstr ", "

#: src/darksky_net.js:268 src/openweathermap_org.js:445
msgid "Today"
msgstr "Idag"

#: src/darksky_net.js:284 src/openweathermap_org.js:447
msgid "Tomorrow"
msgstr "Imorgon"

#: src/darksky_net.js:286 src/openweathermap_org.js:449
#, javascript-format
msgid "In %d day"
msgid_plural "In %d days"
msgstr[0] "Om %d dag"
msgstr[1] "Om %d dagar"

#: src/openweathermap_org.js:181
msgid "Thunderstorm with light rain"
msgstr "Åska och lätt regn"

#: src/openweathermap_org.js:183
msgid "Thunderstorm with rain"
msgstr "Åska och måttligt regn"

#: src/openweathermap_org.js:185
msgid "Thunderstorm with heavy rain"
msgstr "Åska och kraftigt regn"

#: src/openweathermap_org.js:187
msgid "Light thunderstorm"
msgstr "Lätt åska"

#: src/openweathermap_org.js:189
msgid "Thunderstorm"
msgstr "Åska"

#: src/openweathermap_org.js:191
msgid "Heavy thunderstorm"
msgstr "Kraftig åska"

#: src/openweathermap_org.js:193
msgid "Ragged thunderstorm"
msgstr "Spridda åskskurar"

#: src/openweathermap_org.js:195
msgid "Thunderstorm with light drizzle"
msgstr "Åska och lätt duggregn"

#: src/openweathermap_org.js:197
msgid "Thunderstorm with drizzle"
msgstr "Åska och duggregn"

#: src/openweathermap_org.js:199
msgid "Thunderstorm with heavy drizzle"
msgstr "Åska och intensivt duggregn"

#: src/openweathermap_org.js:201
msgid "Light intensity drizzle"
msgstr "Lätt duggregn"

#: src/openweathermap_org.js:203
msgid "Drizzle"
msgstr "Duggregn"

#: src/openweathermap_org.js:205
msgid "Heavy intensity drizzle"
msgstr "Intensivt duggregn"

#: src/openweathermap_org.js:207
msgid "Light intensity drizzle rain"
msgstr "Lätt duggregn och regn"

#: src/openweathermap_org.js:209
msgid "Drizzle rain"
msgstr "Duggregn och regn"

#: src/openweathermap_org.js:211
msgid "Heavy intensity drizzle rain"
msgstr "Intensivt duggregn och regn"

#: src/openweathermap_org.js:213
msgid "Shower rain and drizzle"
msgstr "Regnskurar och duggregn"

#: src/openweathermap_org.js:215
msgid "Heavy shower rain and drizzle"
msgstr "Kraftiga regnskurar och duggregn"

#: src/openweathermap_org.js:217
msgid "Shower drizzle"
msgstr "Tidvis duggregn"

#: src/openweathermap_org.js:219
msgid "Light rain"
msgstr "Lätt regn"

#: src/openweathermap_org.js:221
msgid "Moderate rain"
msgstr "Måttligt regn"

#: src/openweathermap_org.js:223
msgid "Heavy intensity rain"
msgstr "Kraftigt regn"

#: src/openweathermap_org.js:225
msgid "Very heavy rain"
msgstr "Mycket kraftigt regn"

#: src/openweathermap_org.js:227
msgid "Extreme rain"
msgstr "Rikligt med regn"

#: src/openweathermap_org.js:229
msgid "Freezing rain"
msgstr "Underkylt regn"

#: src/openweathermap_org.js:231
msgid "Light intensity shower rain"
msgstr "Lätta regnskurar"

#: src/openweathermap_org.js:233
msgid "Shower rain"
msgstr "Regnskurar"

#: src/openweathermap_org.js:235
msgid "Heavy intensity shower rain"
msgstr "Kraftiga regnskurar"

#: src/openweathermap_org.js:237
msgid "Ragged shower rain"
msgstr "Någon regnskur"

#: src/openweathermap_org.js:239
msgid "Light snow"
msgstr "Lätt snöfall"

#: src/openweathermap_org.js:241
msgid "Snow"
msgstr "Måttligt snöfall"

#: src/openweathermap_org.js:243
msgid "Heavy snow"
msgstr "Kraftigt snöfall"

#: src/openweathermap_org.js:245
msgid "Sleet"
msgstr "Snöblandat regn"

#: src/openweathermap_org.js:247
msgid "Shower sleet"
msgstr "Skurar av snöblandat regn"

#: src/openweathermap_org.js:249
msgid "Light rain and snow"
msgstr "Lätt snöblandat regn"

#: src/openweathermap_org.js:251
msgid "Rain and snow"
msgstr "Måttligt snöblandat regn"

#: src/openweathermap_org.js:253
msgid "Light shower snow"
msgstr "Lätta snöbyar"

#: src/openweathermap_org.js:255
msgid "Shower snow"
msgstr "Snöbyar"

#: src/openweathermap_org.js:257
msgid "Heavy shower snow"
msgstr "Kraftiga snöbyar"

#: src/openweathermap_org.js:259
msgid "Mist"
msgstr "Fuktdis"

#: src/openweathermap_org.js:261
msgid "Smoke"
msgstr "Rök"

#: src/openweathermap_org.js:263
msgid "Haze"
msgstr "Torrdis"

#: src/openweathermap_org.js:265
msgid "Sand/Dust Whirls"
msgstr "Virvlar av sand eller stoft"

#: src/openweathermap_org.js:267
msgid "Fog"
msgstr "Dimma"

#: src/openweathermap_org.js:269
msgid "Sand"
msgstr "Sand"

#: src/openweathermap_org.js:271
msgid "Dust"
msgstr "Stoft"

#: src/openweathermap_org.js:273
msgid "VOLCANIC ASH"
msgstr "VULKANISK ASKA"

#: src/openweathermap_org.js:275
msgid "SQUALLS"
msgstr "LINJEBYAR"

#: src/openweathermap_org.js:277
msgid "TORNADO"
msgstr "TORNADO"

#: src/openweathermap_org.js:279
msgid "Sky is clear"
msgstr "Klart"

#: src/openweathermap_org.js:281
msgid "Few clouds"
msgstr "Nästan klart"

#: src/openweathermap_org.js:283
msgid "Scattered clouds"
msgstr "Halvklart"

#: src/openweathermap_org.js:285
msgid "Broken clouds"
msgstr "Mycket moln"

#: src/openweathermap_org.js:287
msgid "Overcast clouds"
msgstr "Mulet"

#: src/openweathermap_org.js:289
msgid "Not available"
msgstr "Inte tillgänglig"

#: src/prefs.js:201 src/prefs.js:257 src/prefs.js:305 src/prefs.js:312
#, javascript-format
msgid "Invalid data when searching for \"%s\""
msgstr "Felaktig data vid sökning efter \"%s\""

#: src/prefs.js:209 src/prefs.js:273 src/prefs.js:318
#, javascript-format
msgid "\"%s\" not found"
msgstr "Kunde inte hitta \"%s\""

#: src/prefs.js:379
msgid "Location"
msgstr "Plats"

#: src/prefs.js:389
msgid "Provider"
msgstr "Leverantör"

#: src/prefs.js:538
#, javascript-format
msgid "Remove %s ?"
msgstr "Ta bort %s?"

#: src/prefs.js:1071
msgid "default"
msgstr "standard"

#: data/weather-settings.ui:31
msgid "Edit name"
msgstr "Redigera namn"

#: data/weather-settings.ui:49 data/weather-settings.ui:79
#: data/weather-settings.ui:224
msgid "Clear entry"
msgstr "Töm fält"

#: data/weather-settings.ui:63
msgid "Edit coordinates"
msgstr "Redigera koordinater"

#: data/weather-settings.ui:93 data/weather-settings.ui:261
msgid "Extensions default weather provider"
msgstr "Tilläggets standardleverantör av väder"

#: data/weather-settings.ui:125 data/weather-settings.ui:293
msgid "Cancel"
msgstr "Avbryt"

#: data/weather-settings.ui:143 data/weather-settings.ui:311
msgid "Save"
msgstr "Spara"

#: data/weather-settings.ui:201
msgid "Search by location or coordinates"
msgstr "Sök med platsnamn eller koordinater"

#: data/weather-settings.ui:225
msgid "e.g. Vaiaku, Tuvalu or -8.5211767,179.1976747"
msgstr "t.ex. Vaiaku, Tuvalu or -8.5211767,179.1976747"

#: data/weather-settings.ui:235
msgid "Find"
msgstr "Sök"

#: data/weather-settings.ui:468
msgid "Chose default weather provider"
msgstr "Välj standardleverantör av väder"

#: data/weather-settings.ui:481
msgid "Personal Api key from openweathermap.org"
msgstr "Personlig API-nyckel för openweathermap.org"

#: data/weather-settings.ui:532
msgid "Personal Api key from Dark Sky"
msgstr "Personlig API-nyckel för Dark Sky"

#: data/weather-settings.ui:545
msgid "Refresh timeout for current weather [min]"
msgstr "Uppdatera timeout för aktuellt väder [min]"

#: data/weather-settings.ui:559
msgid "Refresh timeout for weather forecast [min]"
msgstr "Uppdatera timeout för väderprognos [min]"

#: data/weather-settings.ui:587
msgid ""
"Note: the forecast-timout is not used for Dark Sky, because they do not "
"provide seperate downloads for current weather and forecasts."
msgstr "Notera att timeout för väderprognos ej gäller för Dark Sky. "
"Aktuellt väder och väderprognos hämtas där samtidigt."

#: data/weather-settings.ui:615
msgid "Use extensions api-key for openweathermap.org"
msgstr "Använd tilläggets API-nyckel för openweathermap.org"

#: data/weather-settings.ui:626
msgid ""
"Switch off, if you have your own api-key for openweathermap.org and put it "
"into the text-box below."
msgstr "Stäng av om du har en egen API-nyckel för openweathermap.org. "
"Lägg sedan in den i textrutan under."

#: data/weather-settings.ui:643
msgid "Weather provider"
msgstr "Väderleverantör"

#: data/weather-settings.ui:663
msgid "Chose geolocation provider"
msgstr "Välj leverantör av geografisk platsbestämning"

#: data/weather-settings.ui:689
msgid "Personal AppKey from developer.mapquest.com"
msgstr "Personlig API-nyckel för developer.mapquest.com"

#: data/weather-settings.ui:718
msgid "Geolocation provider"
msgstr "Geografisk platsbestämning"

#: data/weather-settings.ui:738
#: data/org.gnome.shell.extensions.openweather.gschema.xml:63
msgid "Temperature Unit"
msgstr "Temperatur"

#: data/weather-settings.ui:749
msgid "Wind Speed Unit"
msgstr "Vindhastighet"

#: data/weather-settings.ui:760
#: data/org.gnome.shell.extensions.openweather.gschema.xml:67
msgid "Pressure Unit"
msgstr "Lufttryck"

#: data/weather-settings.ui:797
msgid "Beaufort"
msgstr "Beaufort"

#: data/weather-settings.ui:837
msgid "Units"
msgstr "Enheter"

#: data/weather-settings.ui:857
#: data/org.gnome.shell.extensions.openweather.gschema.xml:113
msgid "Position in Panel"
msgstr "Placering i panelen"

#: data/weather-settings.ui:868
msgid "Position of menu-box [%] from 0 (left) to 100 (right)"
msgstr "Placering i menyrutan [%] från 0 (vänster) till 100 (höger)"

#: data/weather-settings.ui:879
#: data/org.gnome.shell.extensions.openweather.gschema.xml:76
msgid "Wind Direction by Arrows"
msgstr "Visa vindriktning som pilar"

#: data/weather-settings.ui:890
#: data/org.gnome.shell.extensions.openweather.gschema.xml:89
msgid "Translate Conditions"
msgstr "Översätt väderförhållanden"

#: data/weather-settings.ui:901
#: data/org.gnome.shell.extensions.openweather.gschema.xml:93
msgid "Symbolic Icons"
msgstr "Symboliska ikoner"

#: data/weather-settings.ui:912
msgid "Text on buttons"
msgstr "Text på knappar"

#: data/weather-settings.ui:923
#: data/org.gnome.shell.extensions.openweather.gschema.xml:101
msgid "Temperature in Panel"
msgstr "Temperatur i panel"

#: data/weather-settings.ui:934
#: data/org.gnome.shell.extensions.openweather.gschema.xml:105
msgid "Conditions in Panel"
msgstr "Väderförhållanden i panel"

#: data/weather-settings.ui:945
#: data/org.gnome.shell.extensions.openweather.gschema.xml:109
msgid "Conditions in Forecast"
msgstr "Väderförhållanden i prognos"

#: data/weather-settings.ui:956
msgid "Center forecast"
msgstr "Centrera prognos"

#: data/weather-settings.ui:967
#: data/org.gnome.shell.extensions.openweather.gschema.xml:133
msgid "Number of days in forecast"
msgstr "Antal dagar i prognos"

#: data/weather-settings.ui:978
#: data/org.gnome.shell.extensions.openweather.gschema.xml:137
msgid "Maximal number of digits after the decimal point"
msgstr "Maximalt antal decimaler"

#: data/weather-settings.ui:990
msgid "Center"
msgstr "Mitten"

#: data/weather-settings.ui:991
msgid "Right"
msgstr "Höger"

#: data/weather-settings.ui:992
msgid "Left"
msgstr "Vänster"

#: data/weather-settings.ui:1150
msgid "Layout"
msgstr "Utseende"

#: data/weather-settings.ui:1202
msgid "Version: "
msgstr "Version: "

#: data/weather-settings.ui:1216
msgid "unknown (self-build ?)"
msgstr "okänd (privatbygge?)"

#: data/weather-settings.ui:1236
msgid ""
"<span>Weather extension to display weather information from <a href="
"\"https://openweathermap.org/\">Openweathermap</a> or <a href=\"https://"
"darksky.net\">Dark Sky</a> for almost all locations in the world.</span>"
msgstr ""
"<span>Tillägg för att visa väderinformation från <a href="
"\"https://openweathermap.org/\">Openweathermap</a> eller <a href=\"https://"
"darksky.net\">Dark Sky</a> för nästan alla platser i världen.</span>"

#: data/weather-settings.ui:1259
msgid "Maintained by"
msgstr "Underhålls av"

#: data/weather-settings.ui:1289
msgid "Webpage"
msgstr "Hemsida"

#: data/weather-settings.ui:1309
msgid ""
"<span size=\"small\">This program comes with ABSOLUTELY NO WARRANTY.\n"
"See the <a href=\"https://www.gnu.org/licenses/old-licenses/gpl-2.0.html"
"\">GNU General Public License, version 2 or later</a> for details.</span>"
msgstr "<span size=\"small\">Detta program kommer HELT UTAN GARANTI.\n"
"Se <a href=\"https://www.gnu.org/licenses/old-licenses/gpl-2.0.html\">"
"GNU General Public License, version 2 eller senare</a> för detaljer.</span>"

#: data/weather-settings.ui:1330
msgid "About"
msgstr "Om"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:55
msgid "Weather Provider"
msgstr "Väderleverantör"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:59
msgid "Geolocation Provider"
msgstr "Leverantör av geografisk platsbestämning"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:71
msgid "Wind Speed Units"
msgstr "Enheter för vindhastighet"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:72
msgid ""
"Choose the units used for wind speed. Allowed values are 'kph', 'mph', 'm/"
"s', 'knots', 'ft/s' or 'Beaufort'."
msgstr "Välj enhet för vindstyrka. Tillåtna värden är 'kph', 'mph', 'm/"
"s', 'knots', 'ft/s' eller 'Beaufort'."

#: data/org.gnome.shell.extensions.openweather.gschema.xml:77
msgid "Choose whether to display wind direction through arrows or letters."
msgstr "Välj om vindriktning ska visas som pilar eller bokstäver."

#: data/org.gnome.shell.extensions.openweather.gschema.xml:81
msgid "City to be displayed"
msgstr "Plats som ska visas"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:85
msgid "Actual City"
msgstr "Vald plats"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:97
msgid "Use text on buttons in menu"
msgstr "Visa text på menyknappar"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:117
msgid "Horizontal position of menu-box."
msgstr "Horisontell placering av menyruta."

#: data/org.gnome.shell.extensions.openweather.gschema.xml:121
msgid "Refresh interval (actual weather)"
msgstr "Uppdateringsfrekvens (väder)"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:125
msgid "Refresh interval (forecast)"
msgstr "Uppdateringsfrekvens (prognos)"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:129
msgid "Center forecastbox."
msgstr "Centrera prognosruta."

#: data/org.gnome.shell.extensions.openweather.gschema.xml:141
msgid "Your personal API key from openweathermap.org"
msgstr "Personlig API-nyckel för openweathermap.org"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:145
msgid "Use the extensions default API key from openweathermap.org"
msgstr "Använd tilläggets standard-API-nyckel för openweathermap.org"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:149
msgid "Your personal API key from Dark Sky"
msgstr "Personlig API-nyckel för Dark Sky"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:153
msgid "Your personal AppKey from developer.mapquest.com"
msgstr "Personlig API-nyckel för developer.mapquest.com"
