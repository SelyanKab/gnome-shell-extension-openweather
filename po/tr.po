# German translation for gnome-shell-extension-openweather
# Copyright (C) 2011-2015 Jens Lody
# This file is distributed under the same license as the gnome-shell-extension-openweather package.
# Orhan Engin OKAY <orhanenginokay@hotmail.com>, 2018
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-shell-extension-openweather\n"
"Report-Msgid-Bugs-To: https://gitlab.com/jenslody/gnome-shell-extension-"
"openweather/issues\n"
"POT-Creation-Date: 2017-04-02 23:47+0200\n"
"PO-Revision-Date: 2018-10-17 20:28+0300\n"
"Last-Translator: Orhan Engin OKAY <orhanenginokay@hotmail.com>\n"
"Language-Team: Turkish\n"
"Language: tr_TR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 2.0.6\n"

#: src/extension.js:174
msgid "..."
msgstr "..."

#: src/extension.js:384
msgid ""
"Openweathermap.org does not work without an api-key.\n"
"Either set the switch to use the extensions default key in the preferences "
"dialog to on or register at https://openweathermap.org/appid and paste your "
"personal key into the preferences dialog."
msgstr ""
"Openweathermap.org api-anahtarı olmadan çalışmaz.\n"
"İstereniz eklentinin varsayılan anahtarını kullanabilir ya da https://"
"openweathermap.org/appid adresine kayıt olup kendi kişisel anahtarınızı "
"ekleyebilirsiniz."

#: src/extension.js:442
msgid ""
"Dark Sky does not work without an api-key.\n"
"Please register at https://darksky.net/dev/register and paste your personal "
"key into the preferences dialog."
msgstr ""
"Dark Sky Api-anahtarı olmadan çalışmaz.\n"
"Lütfen https://darksky.net/dev/register kayıt olun ve kişisel anahtarınızı "
"ekleyin."

#: src/extension.js:515
#, javascript-format
msgid "Can not connect to %s"
msgstr " %s bağlanılamadı."

#: src/extension.js:861 data/weather-settings.ui:447
msgid "Locations"
msgstr "Konumlar"

#: src/extension.js:876
msgid "Reload Weather Information"
msgstr "Hava Bilgisini Geri Yükle"

#: src/extension.js:891
msgid "Weather data provided by:"
msgstr "Hava durumu veri sağlayıcısı:"

#: src/extension.js:907
#, javascript-format
msgid "Can not open %s"
msgstr "%s açılamıyor"

#: src/extension.js:914
msgid "Weather Settings"
msgstr "Hava Durumu Ayarları"

#: src/extension.js:978 src/prefs.js:1036
msgid "Invalid city"
msgstr "Geçersiz Şehir"

#: src/extension.js:989
msgid "Invalid location! Please try to recreate it."
msgstr "Geçersiz konum! Lütfen yeniden oluşturmayı deneyin."

#: src/extension.js:1036 data/weather-settings.ui:773
msgid "°F"
msgstr "°F"

#: src/extension.js:1038 data/weather-settings.ui:774
msgid "K"
msgstr "K"

#: src/extension.js:1040 data/weather-settings.ui:775
msgid "°Ra"
msgstr "°Ra"

#: src/extension.js:1042 data/weather-settings.ui:776
msgid "°Ré"
msgstr "°Ré"

#: src/extension.js:1044 data/weather-settings.ui:777
msgid "°Rø"
msgstr "°Rø"

#: src/extension.js:1046 data/weather-settings.ui:778
msgid "°De"
msgstr "°De"

#: src/extension.js:1048 data/weather-settings.ui:779
msgid "°N"
msgstr "°N"

#: src/extension.js:1050 data/weather-settings.ui:772
msgid "°C"
msgstr "°C"

#: src/extension.js:1091
msgid "Calm"
msgstr "Sakin"

#: src/extension.js:1094
msgid "Light air"
msgstr "Hafif Hava"

#: src/extension.js:1097
msgid "Light breeze"
msgstr "Hafif Esinti"

#: src/extension.js:1100
msgid "Gentle breeze"
msgstr "Hafif Esinti"

#: src/extension.js:1103
msgid "Moderate breeze"
msgstr "Ilımlı Esinti"

#: src/extension.js:1106
msgid "Fresh breeze"
msgstr "Serin Esinti"

#: src/extension.js:1109
msgid "Strong breeze"
msgstr "Güçlü Esinti"

#: src/extension.js:1112
msgid "Moderate gale"
msgstr "Ilımlı Rüzgar"

#: src/extension.js:1115
msgid "Fresh gale"
msgstr "Serin Rüzgar"

#: src/extension.js:1118
msgid "Strong gale"
msgstr "Güçlü Rüzgar"

#: src/extension.js:1121
msgid "Storm"
msgstr "Fırtına"

#: src/extension.js:1124
msgid "Violent storm"
msgstr "Şiddetli Fırtına"

#: src/extension.js:1127
msgid "Hurricane"
msgstr "Kasırga"

#: src/extension.js:1131
msgid "Sunday"
msgstr "Pazar"

#: src/extension.js:1131
msgid "Monday"
msgstr "Pazartesi"

#: src/extension.js:1131
msgid "Tuesday"
msgstr "Salı"

#: src/extension.js:1131
msgid "Wednesday"
msgstr "Çarşamba"

#: src/extension.js:1131
msgid "Thursday"
msgstr "Perşembe"

#: src/extension.js:1131
msgid "Friday"
msgstr "Cuma"

#: src/extension.js:1131
msgid "Saturday"
msgstr "Çumartesi"

#: src/extension.js:1137
msgid "N"
msgstr "K"

#: src/extension.js:1137
msgid "NE"
msgstr "KD"

#: src/extension.js:1137
msgid "E"
msgstr "D"

#: src/extension.js:1137
msgid "SE"
msgstr "GD"

#: src/extension.js:1137
msgid "S"
msgstr "G"

#: src/extension.js:1137
msgid "SW"
msgstr "GB"

#: src/extension.js:1137
msgid "W"
msgstr "B"

#: src/extension.js:1137
msgid "NW"
msgstr "KB"

#: src/extension.js:1230 src/extension.js:1239 data/weather-settings.ui:810
msgid "hPa"
msgstr "hPa"

#: src/extension.js:1234 data/weather-settings.ui:811
msgid "inHg"
msgstr "inHg"

#: src/extension.js:1244 data/weather-settings.ui:812
msgid "bar"
msgstr "bar"

#: src/extension.js:1249 data/weather-settings.ui:813
msgid "Pa"
msgstr "Pa"

#: src/extension.js:1254 data/weather-settings.ui:814
msgid "kPa"
msgstr "kPa"

#: src/extension.js:1259 data/weather-settings.ui:815
msgid "atm"
msgstr "atm"

#: src/extension.js:1264 data/weather-settings.ui:816
msgid "at"
msgstr "at"

#: src/extension.js:1269 data/weather-settings.ui:817
msgid "Torr"
msgstr "Torr"

#: src/extension.js:1274 data/weather-settings.ui:818
msgid "psi"
msgstr "psi"

#: src/extension.js:1279 data/weather-settings.ui:819
msgid "mmHg"
msgstr "mmHg"

#: src/extension.js:1284 data/weather-settings.ui:820
msgid "mbar"
msgstr "mbar"

#: src/extension.js:1328 data/weather-settings.ui:794
msgid "m/s"
msgstr "m/s"

#: src/extension.js:1332 data/weather-settings.ui:793
msgid "mph"
msgstr "mph"

#: src/extension.js:1337 data/weather-settings.ui:792
msgid "km/h"
msgstr "km/h"

#: src/extension.js:1346 data/weather-settings.ui:795
msgid "kn"
msgstr "kn"

#: src/extension.js:1351 data/weather-settings.ui:796
msgid "ft/s"
msgstr "kn"

#: src/extension.js:1442
msgid "Loading ..."
msgstr "Yükleniyor ..."

#: src/extension.js:1446
msgid "Please wait"
msgstr "Lütfen bekleyin"

#: src/extension.js:1507
msgid "Cloudiness:"
msgstr "Bulanıklık:"

#: src/extension.js:1511
msgid "Humidity:"
msgstr "Nem:"

#: src/extension.js:1515
msgid "Pressure:"
msgstr "Basınç:"

#: src/extension.js:1519
msgid "Wind:"
msgstr "Rüzgar:"

#: src/darksky_net.js:159 src/darksky_net.js:288 src/openweathermap_org.js:350
#: src/openweathermap_org.js:451
msgid "Yesterday"
msgstr "Dün"

#: src/darksky_net.js:162 src/darksky_net.js:291 src/openweathermap_org.js:352
#: src/openweathermap_org.js:453
#, javascript-format
msgid "%d day ago"
msgid_plural "%d days ago"
msgstr[0] " %d gün önce"
msgstr[1] "Vor %d Tagen"

#: src/darksky_net.js:174 src/darksky_net.js:176 src/openweathermap_org.js:366
#: src/openweathermap_org.js:368
msgid ", "
msgstr ", "

#: src/darksky_net.js:268 src/openweathermap_org.js:445
msgid "Today"
msgstr "Bugün"

#: src/darksky_net.js:284 src/openweathermap_org.js:447
msgid "Tomorrow"
msgstr "Yarın"

#: src/darksky_net.js:286 src/openweathermap_org.js:449
#, javascript-format
msgid "In %d day"
msgid_plural "In %d days"
msgstr[0] "%d gün içinde"
msgstr[1] "%d gün içinde"

#: src/openweathermap_org.js:181
msgid "Thunderstorm with light rain"
msgstr "Gök Gürültülü Hafif Yağış"

#: src/openweathermap_org.js:183
msgid "Thunderstorm with rain"
msgstr "Gök Gürültülü Yağış"

#: src/openweathermap_org.js:185
msgid "Thunderstorm with heavy rain"
msgstr "Gök Gürültülü Sağnak Yağış"

#: src/openweathermap_org.js:187
msgid "Light thunderstorm"
msgstr "Hafif Fırtına"

#: src/openweathermap_org.js:189
msgid "Thunderstorm"
msgstr "Fırtına"

#: src/openweathermap_org.js:191
msgid "Heavy thunderstorm"
msgstr "Yoğun Fırtına"

#: src/openweathermap_org.js:193
msgid "Ragged thunderstorm"
msgstr "Düzensiz Fırtına"

#: src/openweathermap_org.js:195
msgid "Thunderstorm with light drizzle"
msgstr "Gök Gürültülü Hafif Çiseleme"

#: src/openweathermap_org.js:197
msgid "Thunderstorm with drizzle"
msgstr "Gök Gürültülü Çiseleme"

#: src/openweathermap_org.js:199
msgid "Thunderstorm with heavy drizzle"
msgstr "Gök Gürültülü Yoğun Çiseleme"

#: src/openweathermap_org.js:201
msgid "Light intensity drizzle"
msgstr "Hafif Yoğunlukta Çiseleme"

#: src/openweathermap_org.js:203
msgid "Drizzle"
msgstr "Çiseleme"

#: src/openweathermap_org.js:205
msgid "Heavy intensity drizzle"
msgstr "Yoğun Yoğunlukta Çiseleme"

#: src/openweathermap_org.js:207
msgid "Light intensity drizzle rain"
msgstr "Hafif Yoğunlukla Çiseleyen Yağmur"

#: src/openweathermap_org.js:209
msgid "Drizzle rain"
msgstr "Çiseleyen Yağmur"

#: src/openweathermap_org.js:211
msgid "Heavy intensity drizzle rain"
msgstr "Yoğun Yoğunlukla Çiseleyen Yağmur"

#: src/openweathermap_org.js:213
msgid "Shower rain and drizzle"
msgstr "Hafif Yağmur ve Çiseleme"

#: src/openweathermap_org.js:215
msgid "Heavy shower rain and drizzle"
msgstr "Sağnak Yağmur ve Çiseleme"

#: src/openweathermap_org.js:217
msgid "Shower drizzle"
msgstr "Hafif Çiseleme"

#: src/openweathermap_org.js:219
msgid "Light rain"
msgstr "Hafif Yağmur"

#: src/openweathermap_org.js:221
msgid "Moderate rain"
msgstr "Ilımlı Yağmur"

#: src/openweathermap_org.js:223
msgid "Heavy intensity rain"
msgstr "Sağnak Yoğunlukta Yağmur"

#: src/openweathermap_org.js:225
msgid "Very heavy rain"
msgstr "Çok Şiddetli Yağmur"

#: src/openweathermap_org.js:227
msgid "Extreme rain"
msgstr "Çok Sağnak Yağmur"

#: src/openweathermap_org.js:229
msgid "Freezing rain"
msgstr "Dondurucu Yağmur"

#: src/openweathermap_org.js:231
msgid "Light intensity shower rain"
msgstr "Hafif Yoğuklukta Çiseleyen Yağmur"

#: src/openweathermap_org.js:233
msgid "Shower rain"
msgstr "Hafif Yağmur"

#: src/openweathermap_org.js:235
msgid "Heavy intensity shower rain"
msgstr "Yoğu Yoğunlukta Sağnak Yağmur"

#: src/openweathermap_org.js:237
msgid "Ragged shower rain"
msgstr "Düzensiz Hafif Yağmur"

#: src/openweathermap_org.js:239
msgid "Light snow"
msgstr "Hafif Kar"

#: src/openweathermap_org.js:241
msgid "Snow"
msgstr "Kar"

#: src/openweathermap_org.js:243
msgid "Heavy snow"
msgstr "Yoğun Kar"

#: src/openweathermap_org.js:245
msgid "Sleet"
msgstr "Sulu Kar"

#: src/openweathermap_org.js:247
msgid "Shower sleet"
msgstr "Hafif Yağışlı Sulu Kar"

#: src/openweathermap_org.js:249
msgid "Light rain and snow"
msgstr "Hafif Yağış ve Kar"

#: src/openweathermap_org.js:251
msgid "Rain and snow"
msgstr "Yağış ve Kar"

#: src/openweathermap_org.js:253
msgid "Light shower snow"
msgstr "Hafif Kar Yağışı"

#: src/openweathermap_org.js:255
msgid "Shower snow"
msgstr "Kar Yağışı"

#: src/openweathermap_org.js:257
msgid "Heavy shower snow"
msgstr "Yoğun Kar Yağışı"

#: src/openweathermap_org.js:259
msgid "Mist"
msgstr "Sis"

#: src/openweathermap_org.js:261
msgid "Smoke"
msgstr "Duman"

#: src/openweathermap_org.js:263
msgid "Haze"
msgstr "Pus"

#: src/openweathermap_org.js:265
msgid "Sand/Dust Whirls"
msgstr "Kum/Toz Hortumu"

#: src/openweathermap_org.js:267
msgid "Fog"
msgstr "Sis"

#: src/openweathermap_org.js:269
msgid "Sand"
msgstr "Kum"

#: src/openweathermap_org.js:271
msgid "Dust"
msgstr "Toz"

#: src/openweathermap_org.js:273
msgid "VOLCANIC ASH"
msgstr "VOLKANİK KÜL"

#: src/openweathermap_org.js:275
msgid "SQUALLS"
msgstr "BAĞRIŞMA"

#: src/openweathermap_org.js:277
msgid "TORNADO"
msgstr "HORTUM"

#: src/openweathermap_org.js:279
msgid "Sky is clear"
msgstr "Temiz Hava"

#: src/openweathermap_org.js:281
msgid "Few clouds"
msgstr "Az Bulutlu"

#: src/openweathermap_org.js:283
msgid "Scattered clouds"
msgstr "Dağınık Bulutlu"

#: src/openweathermap_org.js:285
msgid "Broken clouds"
msgstr "Parçalı Bulutlu"

#: src/openweathermap_org.js:287
msgid "Overcast clouds"
msgstr "Bulutlu"

#: src/openweathermap_org.js:289
msgid "Not available"
msgstr "Mevcut değil"

#: src/prefs.js:201 src/prefs.js:257 src/prefs.js:305 src/prefs.js:312
#, javascript-format
msgid "Invalid data when searching for \"%s\""
msgstr " \"%s\" geçersiz veriler"

#: src/prefs.js:209 src/prefs.js:273 src/prefs.js:318
#, javascript-format
msgid "\"%s\" not found"
msgstr "\"%s\" bulunamadı"

#: src/prefs.js:379
msgid "Location"
msgstr "Konum"

#: src/prefs.js:389
msgid "Provider"
msgstr "Sağlayıcı"

#: src/prefs.js:538
#, javascript-format
msgid "Remove %s ?"
msgstr "Kaldır %s ?"

#: src/prefs.js:1071
msgid "default"
msgstr "varsayılan"

#: data/weather-settings.ui:31
msgid "Edit name"
msgstr "Adı düzenle"

#: data/weather-settings.ui:49 data/weather-settings.ui:79
#: data/weather-settings.ui:224
msgid "Clear entry"
msgstr "Girdiyi temizle"

#: data/weather-settings.ui:63
msgid "Edit coordinates"
msgstr "Koordinatları düzenle"

#: data/weather-settings.ui:93 data/weather-settings.ui:261
msgid "Extensions default weather provider"
msgstr "Varsayılan eklenti hava durumu sağlayıcısı"

#: data/weather-settings.ui:125 data/weather-settings.ui:293
msgid "Cancel"
msgstr "İptal"

#: data/weather-settings.ui:143 data/weather-settings.ui:311
msgid "Save"
msgstr "Kaydet"

#: data/weather-settings.ui:201
msgid "Search by location or coordinates"
msgstr "Koordinat ya da konuma göre ara"

#: data/weather-settings.ui:225
msgid "e.g. Vaiaku, Tuvalu or -8.5211767,179.1976747"
msgstr "ör. Vaiaku, Tuvalu oder -8.5211767,179.1976747"

#: data/weather-settings.ui:235
msgid "Find"
msgstr "Bul"

#: data/weather-settings.ui:468
msgid "Chose default weather provider"
msgstr "Varsayılan Hava Durumu Sağlayıcısını Seç"

#: data/weather-settings.ui:481
msgid "Personal Api key from openweathermap.org"
msgstr "openweathermap.org için kişisel API anahtarı"

#: data/weather-settings.ui:532
msgid "Personal Api key from Dark Sky"
msgstr "Dark Sky için kişisel API anahtarı"

#: data/weather-settings.ui:545
msgid "Refresh timeout for current weather [min]"
msgstr "Mevcut hava durumu için zaman aşımını yenile [dk]"

#: data/weather-settings.ui:559
msgid "Refresh timeout for weather forecast [min]"
msgstr "Mevcut hava tahmini için zaman aşımını yenile [dk]"

#: data/weather-settings.ui:587
msgid ""
"Note: the forecast-timout is not used for Dark Sky, because they do not "
"provide seperate downloads for current weather and forecasts."
msgstr ""
"Not: hava durumu ve tahminleri için ayrı indirmeler sağlanmadığından, tahmini "
"zaman aşımı Dark Sky için kullanılmaz."

#: data/weather-settings.ui:615
msgid "Use extensions api-key for openweathermap.org"
msgstr "openweathermap.org için uzantı API anahtarını kullan"

#: data/weather-settings.ui:626
msgid ""
"Switch off, if you have your own api-key for openweathermap.org and put it "
"into the text-box below."
msgstr ""
"Eğer openweather.org için kişisel API anahtarınız varsa ve metin kutusuna "
"ekleyebiliyorsanız, Kapatın."

#: data/weather-settings.ui:643
msgid "Weather provider"
msgstr "Hava Durumu Sağlayıcı"

#: data/weather-settings.ui:663
msgid "Chose geolocation provider"
msgstr "Coğrafi konum sağlayıcısı seçin"

#: data/weather-settings.ui:689
msgid "Personal AppKey from developer.mapquest.com"
msgstr "developer.mapquest.com tarafından kişisel uygulama anahtarı"

#: data/weather-settings.ui:718
msgid "Geolocation provider"
msgstr "Coğrafi Konum Sağlayıcısı"

#: data/weather-settings.ui:738
#: data/org.gnome.shell.extensions.openweather.gschema.xml:63
msgid "Temperature Unit"
msgstr "Sıcaklık Birimi"

#: data/weather-settings.ui:749
msgid "Wind Speed Unit"
msgstr "Rüzgar Hız Birimi"

#: data/weather-settings.ui:760
#: data/org.gnome.shell.extensions.openweather.gschema.xml:67
msgid "Pressure Unit"
msgstr "Basınç Birimleri"

#: data/weather-settings.ui:797
msgid "Beaufort"
msgstr "Beaufort Ölçeği"

#: data/weather-settings.ui:837
msgid "Units"
msgstr "Birimler"

#: data/weather-settings.ui:857
#: data/org.gnome.shell.extensions.openweather.gschema.xml:113
msgid "Position in Panel"
msgstr "Panel Pozisyonu"

#: data/weather-settings.ui:868
msgid "Position of menu-box [%] from 0 (left) to 100 (right)"
msgstr "Menü kutusunun konumu [%]  0 (sol) ile 100 (sağ) arasında"

#: data/weather-settings.ui:879
#: data/org.gnome.shell.extensions.openweather.gschema.xml:76
msgid "Wind Direction by Arrows"
msgstr "Ok ile Rüzgar Yönü"

#: data/weather-settings.ui:890
#: data/org.gnome.shell.extensions.openweather.gschema.xml:89
msgid "Translate Conditions"
msgstr "Şartları Çevir"

#: data/weather-settings.ui:901
#: data/org.gnome.shell.extensions.openweather.gschema.xml:93
msgid "Symbolic Icons"
msgstr "Sembolik Simgeler"

#: data/weather-settings.ui:912
msgid "Text on buttons"
msgstr "Düğme Metinleri"

#: data/weather-settings.ui:923
#: data/org.gnome.shell.extensions.openweather.gschema.xml:101
msgid "Temperature in Panel"
msgstr "Paneldeki Sıcaklı"

#: data/weather-settings.ui:934
#: data/org.gnome.shell.extensions.openweather.gschema.xml:105
msgid "Conditions in Panel"
msgstr "Paneldeki Koşullar"

#: data/weather-settings.ui:945
#: data/org.gnome.shell.extensions.openweather.gschema.xml:109
msgid "Conditions in Forecast"
msgstr "Tahmindeki Koşullar"

#: data/weather-settings.ui:956
msgid "Center forecast"
msgstr "Merkez tahmini"

#: data/weather-settings.ui:967
#: data/org.gnome.shell.extensions.openweather.gschema.xml:133
msgid "Number of days in forecast"
msgstr "Tahmin edilen gün sayısı"

#: data/weather-settings.ui:978
#: data/org.gnome.shell.extensions.openweather.gschema.xml:137
msgid "Maximal number of digits after the decimal point"
msgstr "Ondalık noktadan sonraki maksimum basamak sayısı"

#: data/weather-settings.ui:990
msgid "Center"
msgstr "Orta"

#: data/weather-settings.ui:991
msgid "Right"
msgstr "Sağ"

#: data/weather-settings.ui:992
msgid "Left"
msgstr "Sol"

#: data/weather-settings.ui:1150
msgid "Layout"
msgstr "Düzen"

#: data/weather-settings.ui:1202
msgid "Version: "
msgstr "Versiyon:"

#: data/weather-settings.ui:1216
msgid "unknown (self-build ?)"
msgstr "bilinmeyen (kişisel-yapı ?)"

#: data/weather-settings.ui:1236
msgid ""
"<span>Weather extension to display weather information from <a href=\"https://"
"openweathermap.org/\">Openweathermap</a> or <a href=\"https://darksky.net"
"\">Dark Sky</a> for almost all locations in the world.</span>"
msgstr ""
"<span>Hava durumu eklentisi neredeyse dünyadaki tüm konumların hava "
"bilgilerini <a href=\"https://openweathermap.org/\">Openweathermap</a> yada "
"<a href=\"https://darksky.net\">Dark Sky</a> üzerinden sağlar.</span>"

#: data/weather-settings.ui:1259
msgid "Maintained by"
msgstr "Tarafından tutulan"

#: data/weather-settings.ui:1289
msgid "Webpage"
msgstr "Web Sitesi"

#: data/weather-settings.ui:1309
msgid ""
"<span size=\"small\">This program comes with ABSOLUTELY NO WARRANTY.\n"
"See the <a href=\"https://www.gnu.org/licenses/old-licenses/gpl-2.0.html"
"\">GNU General Public License, version 2 or later</a> for details.</span>"
msgstr ""
"<span size=\"small\">Bu program KESİNLİKLE HİÇBİR GARANTİ ile gelmemektedir.\n"
"Daha fazla detay için <a href=\"https://www.gnu.org/licenses/old-licenses/"
"gpl-2.0.html\">GNU General Public License, Version 2 yada sonrasına</a> gidin."
"</span>"

#: data/weather-settings.ui:1330
msgid "About"
msgstr "Hakkında"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:55
msgid "Weather Provider"
msgstr "Hava Durumu Sağlayıcı"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:59
msgid "Geolocation Provider"
msgstr "Coğrafi Konum Sağlayıcısı"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:71
msgid "Wind Speed Units"
msgstr "Rüzgar Hızı Birimleri"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:72
msgid ""
"Choose the units used for wind speed. Allowed values are 'kph', 'mph', 'm/s', "
"'knots', 'ft/s' or 'Beaufort'."
msgstr ""
"Hava hızı için kullanılacak birimleri seçin. İzin verilen değerler 'kph', "
"'mph', 'm/s', 'knots', 'ft/s' oder 'Beaufort'."

#: data/org.gnome.shell.extensions.openweather.gschema.xml:77
msgid "Choose whether to display wind direction through arrows or letters."
msgstr "Rüzgar yönünü oklarla veya harflerle göstermeyi seçin."

#: data/org.gnome.shell.extensions.openweather.gschema.xml:81
msgid "City to be displayed"
msgstr "Gösterilen şehir"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:85
msgid "Actual City"
msgstr "Güncel Şehir"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:97
msgid "Use text on buttons in menu"
msgstr "Menüdeki düğmelerdeki metni kullan"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:117
msgid "Horizontal position of menu-box."
msgstr "Menü kutusunun yatay konumu."

#: data/org.gnome.shell.extensions.openweather.gschema.xml:121
msgid "Refresh interval (actual weather)"
msgstr "Yenileme aralığı (gerçek hava durumu)"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:125
msgid "Refresh interval (forecast)"
msgstr "Yenileme aralığı (tahmini)"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:129
msgid "Center forecastbox."
msgstr "Merkez tahmin kutusu."

#: data/org.gnome.shell.extensions.openweather.gschema.xml:141
msgid "Your personal API key from openweathermap.org"
msgstr "openweathermap.org'dan kişisel API anahtarınız"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:145
msgid "Use the extensions default API key from openweathermap.org"
msgstr ""
"openweathermap.org  adresinden uzantılar için varsayılan API anahtarını "
"kullanın"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:149
msgid "Your personal API key from Dark Sky"
msgstr "Dark Sky'dan Kişisel API anahtarınız"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:153
msgid "Your personal AppKey from developer.mapquest.com"
msgstr "developer.mapquest.com'dan kişisel uygulama anahtarınız"
